def function_8():
    n1 = 10
    n2 = 20

    add = lambda x, y: x + y
    print(f"10 + 10 = {add(10, 10)}")

function_8()

def function_9():
    n1 = 10
    n2 = 20

    add = lambda x, y: x - y
    print(f"10 + 10 = {subtract(10, 10)}")

function_9()

def function_10():
    n1 = 10
    n2 = 20

    add = lambda x, y: x * y
    print(f"10 + 10 = {multiply(10, 10)}")

function_10()

def function_12():
    n1 = 10
    n2 = 20

    add = lambda x, y: x / y
    print(f"10 + 10 = {divide(10, 10)}")

function_12()